
function Square(ctx, x, y, w, h,a) {
  this.ctx = ctx;
  this.x = x;
  this.y = y;
  this.height = h;
  this.width = w;
  this.color = this.getRndColor();
  this.circleColor = this.getRndColor();
  this.radius = 0;
  this.createCircle = Math.random() > 0.65;
  this.decreaseCircle = this.createCircle;
  this.decreaseAlpha = false;
  this.completeAnimation = false;
  this.resetTickCount = 0;

  this.alpha = a; // current alpha for this instance
  this.triggered = false; // is running
  this.doneFill = false; // has finished
  this.doneCircle = false;
  this.ctx.fillStyle = this.color; // render this instance
  this.ctx.globalAlpha = Math.min(1, this.alpha);
  this.ctx.fillRect(this.x, this.y, this.width, this.height);
}


 // prototype methods that will be shared
 Square.prototype = {
  
      trigger: function () { // start this rectangle
        if (!this.triggered) {
          return;
        }

        
        if (this.alpha < 1.0 && !this.decreaseAlpha) {
          this.alpha += 0.1; // update alpha 
          if (this.alpha > 1.0) {
            this.alpha = 1;
            
          }
          //square
          this.ctx.fillStyle = this.color; // render this instance
          this.ctx.globalAlpha = Math.min(1, this.alpha);
          this.ctx.fillRect(this.x, this.y, this.width, this.height);
        } else if (this.radius < (this.width / 2) && this.createCircle) {
          this.radius += 0.2;
          this.ctx.beginPath();
          this.ctx.arc(this.x + (this.width / 2), this.y + (this.width / 2), this.radius, 0, Math.PI * 2, false);
          this.ctx.closePath();
  
  
          this.ctx.fillStyle = this.circleColor;
          this.ctx.fill();
  
          if (this.radius >= (this.width / 4)) {
            this.radius = (this.width / 4);
            this.decreaseCircle = true;
            this.createCircle = false;
          }
        } else if (this.decreaseCircle) {
          this.radius -= .2;
  
          if (this.radius < 0.07) {
            this.radius= 0;
            this.decreaseCircle = false;
            this.decreaseAlpha = true;
          }
  
          this.ctx.beginPath();
          this.ctx.arc(this.x + (this.width / 2), this.y + (this.height / 2), this.radius, 0, Math.PI * 2, false);
          this.ctx.closePath();
  
          this.ctx.fillStyle = this.color; // render this instance
          this.ctx.globalAlpha = Math.min(1, this.alpha);
          this.ctx.fillRect(this.x, this.y, this.width, this.height);
          this.ctx.fillStyle = this.circleColor;
          this.ctx.fill();
        } else if ((this.decreaseAlpha && !this.completeAnimation)) {
  
          this.alpha -= 0.1; // update alpha 
          if (this.alpha <= 0.006) {
            this.alpha = 0.0;
            this.completeAnimation = true;
  
          }
          //square
          this.ctx.clearRect(this.x, this.y, this.width, this.height);
          this.ctx.fillStyle = this.color; // render this instance
          this.ctx.globalAlpha = Math.min(1, this.alpha);
          this.ctx.fillRect(this.x, this.y, this.width, this.height);
        } else {
          this.resetTickCount++;
          if (this.resetTickCount == 540.0) {
            this.createCircle = true; // Math.random() > 0.40;
            this.decreaseCircle = true;
            this.decreaseAlpha = false;
            this.completeAnimation = false;
            this.color = this.getRndColor();
            this.circleColor = this.getRndColor();
            this.resetTickCount = 0;
          }
  
        }
      },
      getRndColor: function() {
        var brandColors = ['#F4F0E6', '#ffffff', '#FFAEAE', '#3FD9C2', '#4c4642', '#FEB62C'];
        var brandColors = ['rgba(255, 255, 255,1)', 'rgba(255, 174, 174,1)',
          'rgba(63, 217, 194,1)', 'rgba(76, 70, 66,1)', 'rgba(254, 182, 44,1)'
        ]
        return brandColors[Math.floor(Math.random() * brandColors.length)];
      }
  
    };