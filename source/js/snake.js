
function Snake(elementName,snakeType){
  this.canvas = document.getElementById(elementName); // get canvas element
  this.ctx = this.canvas.getContext('2d'); // get context
  this.x = 0;
  this.y = 0;
  this.squares = [];
  this.isAnimating = false,
  this.hasActive = true;
  this.snakeType = snakeType; // 0 top snake, 1 bottom snake


  this.canvas.setAttribute('width', window.getComputedStyle(this.canvas, null).getPropertyValue("width"));
  this.canvas.setAttribute('height', window.getComputedStyle(this.canvas, null).getPropertyValue("height"));
  this.squareSize = 22;
}

Snake.prototype = {
  animate: function(){
    if (this.squares.length == 0) {

      var width = this.canvas.offsetWidth
      // if(this.snakeType == 0){
      //   width = width / 2;
      // }
      
      var numberOfGridsNeeded = width / this.squareSize;
      // numberOfGridsNeeded -= 1;
      console.log('we need ' + Math.ceil(numberOfGridsNeeded));
      var ctx2 = this.canvas.getContext('2d');
      ctx2.clearRect(0, 0, this.canvas.width, this.canvas.height);
      for (var i = 0; i < numberOfGridsNeeded; i++) {
        for (var j = 0; j < 3; j++) {
          this.createPixel(i * this.squareSize, j * this.squareSize, ctx2);
        }
      }
    }

    //we're a top snake
    if(this.snakeType == 0){
      return;
    }

    for (var i = 0; i < this.squares.length; i++) {
      if (i == 0 && !this.squares[i].triggered) {
        this.squares[i].triggered = true;
      }
      this.squares[i].trigger();
      if (i + 1 < this.squares.length) {
        if (this.squares[i].alpha > 0.5 && !this.squares[i + 1].triggered) {
          this.squares[i + 1].triggered = true;
        }
      }

    }
  },
  createPixel: function(x,y,context){
    this.squares.push(new Square(context, x, y, this.squareSize, this.squareSize,this.snakeType == 0 ? 1.0 : 0.0))
  }
}

