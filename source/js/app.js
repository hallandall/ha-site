var hallandallApp = {};

class Site {
  constructor() {

  }
}

var requestAnimationFrame = window.requestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.msRequestAnimationFrame;

$(document).ready(function () {

  var topSnake = new Snake('snek', 0);
  var bottomSnake = new Snake('snek-bottom', 1);

  $('#nav-hamburger').on('click', function () {
    if ($(this).hasClass('active')) {
      $(".ha-c-siteHeader__container").toggleClass('hidden');
      $('#nav-hamburger').removeClass('cross');
      $('#ha-c-siteHeader__menu').removeClass('cross');
      setTimeout(function () {
        $('.ha-c-siteHeader__cta').removeClass('active');
        $('#nav-hamburger').removeClass('active');
        $('#ha-c-siteHeader__menu').removeClass('active');
        $(".ha-c-siteHeader__container").toggleClass('hidden');
        // $('.main-site').css('overflow','hidden');
      }, 250);
    } else {
      $('#nav-hamburger').addClass('active');
      $('#ha-c-siteHeader__menu').addClass('active');
      $('.ha-c-siteHeader__cta').addClass('active');
      setTimeout(function () {
        $('#nav-hamburger').addClass('cross');
        $('#ha-c-siteHeader__menu').addClass('cross');
        // $('.main-site').css('overflow','hidden');
      }, 250);
    }
  });

  function checkLastFM() {
    $.getJSON("//ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=jamesh1180&api_key=fef3787a7ef1098803fb4caa58d11dd0&format=json", function (data) {

      if (data['recenttracks']) {

        var trackInfo = data['recenttracks']['track'];

        var currentTrack = trackInfo[0];
        // console.log(currentTrack['@attr'])
        var isPlaying = false;
        if (currentTrack['@attr']) {
          isPlaying = currentTrack['@attr']['nowplaying'];
        }

        var formatString = "";

        if (isPlaying) {
          formatString += "<h3>Currently Rockin':</h3>";
        } else {
          formatString += "<h3>Recently Played:</h3>";
        }
        formatString += "<p class='song-title'>" + currentTrack.name + "</p>";
        formatString += "<img src='" + currentTrack.image[1]['#text'] + "'>";

        formatString += "<p class='song-artist'>by " + currentTrack.artist['#text'] + "</p>";
        $('.currently').html(formatString);



      } else {

      }
      setTimeout(checkLastFM, 10000);
    });
  }

  var hasActive = false;

  function loop() {

    topSnake.animate();
    bottomSnake.animate();

    requestAnimationFrame(loop);

  }

  function checkScroll() {
    var scrollTop = jQuery(document).scrollTop()
    var nav = document.getElementById("main-nav");
    var bodyTag = document.getElementsByTagName("BODY")[0];

    if (scrollTop >= 120 && !$('#ha-c-siteHeader__menu').hasClass('showing')) {
      // $('#nav-hamburger').addClass('active');
      $('#ha-c-siteHeader__menu').addClass('showing');
      $('.ha-c-siteHeader__cta').addClass('showing');
    } else if (scrollTop < 120 && $('#ha-c-siteHeader__menu').hasClass('showing')) {
      // $('#nav-hamburger').removeClass('active');
      $('#ha-c-siteHeader__menu').removeClass('showing');
      $('.ha-c-siteHeader__cta').removeClass('showing');
    }

  }

  $(window).scroll(function (event) {

    if ($(window).width() > 576) {
      return;
    }
    checkScroll(event);

  });

  $("body").mousewheel(function (event, delta) {


    if ($(window).width() < 576) {
      return;
    }
    checkScroll();
    if($('.ha-c-screenshots').length){
      var maxScrollTop = $('.ha-c-screenshots').offset().top + $('.ha-c-screenshots').outerHeight() - window.innerHeight;
      var maxScrollBottom = $('.ha-c-screenshots').offset().top + $('.ha-c-screenshots').outerHeight();
      
      var scrollWidth = document.getElementsByClassName('shot-container')[0].scrollWidth - $(window).width();
      console.log("stopping point: " + maxScrollTop + " current location: " + $(window).scrollTop());
  
      
      if (($(window).scrollTop() >= maxScrollTop) && (($(window).scrollTop() - maxScrollTop) < 50)) {
        $('.shot-container').scrollLeft($('.shot-container').scrollLeft() - delta);
  
        var preventHorizontalScrolling = true;
        if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
          // Scroll up
          if($('.shot-container').scrollLeft() == 0){
            preventHorizontalScrolling = false;
          }
        } else {
          // Scroll down
          if($('.shot-container').scrollLeft() == scrollWidth){
            preventHorizontalScrolling = false;
          }
        }
        if(preventHorizontalScrolling){
          event.preventDefault();
        }
    }
    
      
      return;
    }
    

  });

  checkScroll();
  checkLastFM();
  loop();
});